#!/usr/bin/env bash

pipenv run pyinstaller --name="TeknopolisController" --windowed --paths=tekcontroller --noconfirm \
--osx-bundle-identifier org.dankeenan.teknopolis.controller --icon resources/logo.icns \
--hidden-import='pkg_resources.py2_warn' tekcontroller/__main__.py
