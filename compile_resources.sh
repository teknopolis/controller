#!/usr/bin/env bash

rcc_opts='--verbose --compress-algo zlib --compress 9 --threshold 10'
out_dir='tekcontroller/resources/qt'

mkdir -p "${out_dir}"
pipenv run pyside2-rcc ${rcc_opts} -o ${out_dir}/icons_rcc.py resources/icons.qrc
