#!/usr/bin/env bash

if [[ "$(id -u)" == "0" ]]; then
  echo 'This command must not be run as root!'
  exit 1
else
  echo 'This script will remove configuration from the exhibit computer.'
  echo 'You may be asked for your user password several times.'
fi

# Remove sudoers file
sudo rm -v /etc/sudoers.d/remote_shutdown
echo

# Remove the remoteshutdown group
echo 'Removing the remoteshutdown group'
sudo deluser "$(id -un)" remoteshutdown
sudo delgroup remoteshutdown
echo

# Disable wake-on-lan
interfaces=$(ls /sys/class/net)
for interface in ${interfaces[@]}; do
  if [[ ${interface} != 'lo' ]]; then
    echo "Disabling Wake-on-Lan for ${interface}"
    sudo ethtool -s "${interface}" wol d
  fi
done
echo

echo 'Complete!'
echo 'openssh-server and ethtool remain installed.  Remove them using the package manager.'
echo 'The machine must be rebooted for changes to take effect.'
