#!/usr/bin/env bash
# Device setup script

# Sets $packman to the package manager to use
get_packman() {
  which dnf
  if [[ $? == 0 ]]; then
    packman=dnf
    return
  fi
  which apt-get
  if [[ $? == 0 ]]; then
    packman=apt-get
    return
  fi
  echo 'Could not determine package manager'
  exit 1
}

if [[ "$(id -u)" == "0" ]]; then
  echo 'This command must not be run as root!'
  exit 1
else
  echo 'This script will setup the exhibit computer.'
  echo 'You may be asked for your user password several times.'
fi

# Enable remote shutdown
# Sanity check the added sudoers file, then copy it
echo 'Sanity checking sudoers file'
sudo visudo -c
if [[ $? != 0 ]]; then
  exit 1
fi
sudo visudo -c -f remote_shutdown
if [[ $? != 0 ]]; then
  exit 1
fi
sudo cp -v remote_shutdown /etc/sudoers.d/remote_shutdown
sudo chown -v root:root /etc/sudoers.d/remote_shutdown
sudo chmod -v 0440 /etc/sudoers.d/remote_shutdown
echo

# Create the special remoteshutdown group
echo 'Creating the remoteshutdown group'
sudo addgroup remoteshutdown
echo 'Adding the current user to the remoteshutdown group'
sudo adduser "$(id -un)" remoteshutdown
echo

# Install SSH server
echo 'Installing SSH Server'
get_packman
if [[ ${packman} == 'dnf' ]]; then
  sudo dnf -y install openssh-server ethtool --setopt=install_weak_deps=False
elif [[ ${packman} == 'apt-get' ]]; then
  sudo apt-get install -y --no-install-recommends openssh-server ethtool
fi
echo

# Enable wake-on-lan
interfaces=$(ls /sys/class/net)
for interface in ${interfaces[@]}; do
  if [[ ${interface} != 'lo' ]]; then
    echo "Enabling Wake-on-Lan for ${interface}"
    sudo ethtool -s "${interface}" wol g
  fi
done
echo

echo 'Complete!'
echo 'The machine must be rebooted for changes to take effect.'
