#!/usr/bin/env bash

## Enable SSH
systemsetup -setremotelogin on
sed -E -e $'s/%admin\tALL=\(ALL\) ALL/%admin\tALL=(ALL) NOPASSWD: ALL/' -i original /etc/sudoers

## Enable VNC Server
/System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate -configure -access -on \
    -configure -allowAccessFor -allUsers \
    -configure -clientopts -setvnclegacy -vnclegacy yes \
    -configure -clientopts -setvncpw -vncpw "1234@bam" \
    -configure -restart -agent -privs -all
