#!/usr/bin/env bash

if [ -e "${HOME}/tek_start.app" ] && [ -e "${HOME}/tek_stop.app" ]
then
    echo "Template files exist"
    exitcode=1
else
    echo "Template files don't exist"
    exitcode=0
fi

exit ${exitcode}
