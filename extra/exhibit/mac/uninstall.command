#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]
then
    script_name="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/$0";
    sudo "${0}";
else
    ## Forget these receipts but don't actually do anything
    pkgutil --forget org.dankeenan.teknopolis.scripts
    pkgutil --forget org.dankeenan.teknopolis.exhibit

    ## Disable SSH
    echo "Disabling SSH";
    systemsetup -f -setremotelogin off;
    ## Disable VNC Server
    echo "Disabling VNC";
    /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -deactivate -configure -access -off;
    ## Disable passwordless sudo
    echo "Disabling passwordless privilege escalation...";
    cp /etc/sudoers /etc/sudoers.original;
    sed -E -e $'s/%admin\tALL=\(ALL\) NOPASSWD: ALL/%admin\tALL=(ALL) ALL/' /etc/sudoers.original > /etc/sudoers;
    echo "Done";
    pkgutil --forget org.dankeenan.teknopolis.remote

    ## Disable WOL
    echo "Disabling WOL";
    systemsetup -setwakeonnetworkaccess off;
    pkgutil --forget org.dankeenan.teknopolis.wol
fi
