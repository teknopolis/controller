Unicode True
!define PRODUCT_NAME "Teknopolis Exhibit"
!define PRODUCT_VERSION "2020.0"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"

SetCompressor bzip2

; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
; Install things
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; Reserve files
!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

; MUI end ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "Teknopolis Exhibit Setup.exe"
InstallDir "$PROGRAMFILES\Teknopolis Exhibit"
ShowInstDetails show
ShowUnInstDetails show
RequestExecutionLevel admin

Function .onRebootFailed
    MessageBox MB_OK|MB_ICONSTOP "Reboot failed. Please reboot manually." /SD IDOK
FunctionEnd

Section "MainSection" SEC01
    WriteRegDWORD HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" "LocalAccountTokenFilterPolicy" 0x00000001
    ExecWait '"sc config RemoteRegistry start= auto"'
    DetailPrint "Enabled remote elevation"
    SetRebootFlag true
SectionEnd

Section -AdditionalIcons
    SetOutPath $INSTDIR
    CreateDirectory "$SMPROGRAMS\Teknopolis Exhibit"
    CreateShortCut "$SMPROGRAMS\Teknopolis Exhibit\Uninstall.lnk" "$INSTDIR\uninst.exe"
SectionEnd

Section -Post
    WriteUninstaller "$INSTDIR\uninst.exe"
    WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
    WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"
    WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"

    IfRebootFlag 0 noreboot
      MessageBox MB_YESNO "A reboot is required to finish the installation. Do you wish to reboot now?" IDNO noreboot
        Reboot
    noreboot:
SectionEnd


Function un.onUninstSuccess
    HideWindow
    IfRebootFlag 0 noreboot
        MessageBox MB_YESNO "A reboot is required to finish removing configuration. Do you wish to reboot now?" IDNO noreboot
            Reboot
    noreboot:
        MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
FunctionEnd

Function un.onInit
    MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
    Abort
FunctionEnd

Section Uninstall
    DeleteRegValue HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" "LocalAccountTokenFilterPolicy"
    ExecWait '"sc stop RemoteRegistry"'
    ExecWait '"sc config RemoteRegistry start= disabled"'
    DetailPrint "Disabled remote elevation."
    SetRebootFlag true

    Delete "$INSTDIR\uninst.exe"

    Delete "$SMPROGRAMS\Teknopolis Exhibit\Uninstall.lnk"

    RMDir "$SMPROGRAMS\Teknopolis Exhibit"

    DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
SectionEnd
