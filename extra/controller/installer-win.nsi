; This script requires pandoc to be available in the PATH
Unicode True
!define PRODUCT_NAME "Teknopolis Controller"
!define PRODUCT_VERSION "2020.0"
!define PRODUCT_PUBLISHER "Dan Keenan"
!define PRODUCT_WEB_SITE "https://teknopolis.gitlab.io/controller/"
!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\TeknopolisController.exe"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"
!define PRODUCT_UNINST_ROOT_KEY "HKLM"
!define PRODUCT_UNINST_PATH "uninst.exe"
!define PRODUCT_ENTRYPOINT "TeknopolisController.exe"

SetCompressor bzip2

; Generate usable license and readme files
!tempfile READMETEMP
!system 'pandoc ../../README.md -f commonmark -t html -s --metadata title="Teknopolis Controller Readme" -o "${READMETEMP}"'
!tempfile LICENSETEMP
!system 'pandoc ../../LICENSE.md -f commonmark -t rtf -s -o "${LICENSETEMP}"'

; MUI 1.67 compatible ------
!include "MUI.nsh"

; MUI Settings
!define MUI_ABORTWARNING
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

; Welcome page
!insertmacro MUI_PAGE_WELCOME
; License page
!insertmacro MUI_PAGE_LICENSE "${LICENSETEMP}"
; Directory page
!insertmacro MUI_PAGE_DIRECTORY
; Instfiles page
!insertmacro MUI_PAGE_INSTFILES
; Finish page
!define MUI_FINISHPAGE_RUN "$INSTDIR\TeknopolisController.exe"
!define MUI_FINISHPAGE_SHOWREADME "$INSTDIR\README.html"
!insertmacro MUI_PAGE_FINISH

; Uninstaller pages
!insertmacro MUI_UNPAGE_INSTFILES

; Language files
!insertmacro MUI_LANGUAGE "English"

; Reserve files
!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

; MUI end ------

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "Teknopolis Controller Setup.exe"
InstallDir "$PROGRAMFILES\Teknopolis Controller"
InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""
ShowInstDetails show
ShowUnInstDetails show
RequestExecutionLevel admin

Section "MainSection" SEC01
  SetOutPath "$INSTDIR"
  SetOverwrite ifnewer
  File /r "..\..\dist\TeknopolisController\*.*"
  CreateDirectory "$SMPROGRAMS\Teknopolis Controller"
  CreateShortCut "$SMPROGRAMS\Teknopolis Controller\Teknopolis Controller.lnk" "$INSTDIR\${PRODUCT_ENTRYPOINT}"
  CreateShortCut "$DESKTOP\Teknopolis Controller.lnk" "$INSTDIR\${PRODUCT_ENTRYPOINT}"
  File "/oname=README.html" "${READMETEMP}"
  File "/oname=LICENSE.rtf" "${LICENSETEMP}"
SectionEnd

Section -AdditionalIcons
  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  CreateShortCut "$SMPROGRAMS\Teknopolis Controller\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
  CreateShortCut "$SMPROGRAMS\Teknopolis Controller\Uninstall.lnk" "$INSTDIR\uninst.exe"
SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\${PRODUCT_UNINST_PATH}"
  CreateShortCut "$SMPROGRAMS\Teknopolis Controller\Uninstall.lnk" "$INSTDIR\${PRODUCT_UNINST_PATH}"
  WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\${PRODUCT_ENTRYPOINT}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\${PRODUCT_UNINST_PATH}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\${PRODUCT_ENTRYPOINT}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"
SectionEnd


Function un.onUninstSuccess
  HideWindow
  MessageBox MB_ICONINFORMATION|MB_OK "$(^Name) was successfully removed from your computer."
FunctionEnd

Function un.onInit
  MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "Are you sure you want to completely remove $(^Name) and all of its components?" IDYES +2
  Abort
FunctionEnd

Section Uninstall
  Delete "$SMPROGRAMS\Teknopolis Controller\Uninstall.lnk"
  Delete "$SMPROGRAMS\Teknopolis Controller\Website.lnk"
  Delete "$DESKTOP\Teknopolis Controller.lnk"
  Delete "$SMPROGRAMS\Teknopolis Controller\Teknopolis Controller.lnk"

  RMDir "$SMPROGRAMS\Teknopolis Controller"
  RMDir /r "$INSTDIR"

  ; Remove config only if the user wants to
  MessageBox MB_YESNO|MB_ICONQUESTION "Remove configuration?" /SD IDNO IDYES unconfig IDNO unkeepconfig
  unconfig:
    DeleteRegKey HKCU "SOFTWARE\Dan Keenan\TeknopolisController"
    DeleteRegKey /ifempty HKCU "SOFTWARE\Dan Keenan"
    DetailPrint "Removed configuration."
  unkeepconfig:

  DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"
  DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"
SectionEnd
