#!/usr/bin/env bash

out_dir='tekcontroller/ui/uic'

mkdir -p "${out_dir}"
pipenv run pyside2-uic --no-autoconnection ui/MainWindow.ui -o ${out_dir}/ui_mainwindow.py
sed -i -e 's/^import icons_rc$//gm' ${out_dir}/ui_mainwindow.py
