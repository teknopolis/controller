from typing import Dict

from PySide2.QtCore import Signal
from PySide2.QtGui import QCloseEvent
from PySide2.QtWidgets import QDialog, QDialogButtonBox, QFormLayout, QMessageBox, QProgressDialog, QPushButton, \
    QTabWidget, QVBoxLayout, QWidget

from devices.device import Device, DeviceTypeRegistry


class EditDeviceDialog(QDialog):
    """
    Add device dialog box
    """
    field_widgets: Dict[str, Dict[str, QWidget]] = {}
    tabs: Dict[str, QWidget] = {}
    _editing_device = None
    _progress: QProgressDialog = None

    # Signals
    device_added = Signal(Device)
    device_edited = Signal(Device)

    def __init__(self, parent: QWidget):
        QDialog.__init__(self, parent)
        self._init_ui()

    def _init_ui(self):
        self.setWindowTitle('Edit Device')
        self.layout = QVBoxLayout(self)
        self.setLayout(self.layout)
        self.cancel_editing_button = QPushButton('Edit mode - click to cancel', self)
        self.cancel_editing_button.setVisible(False)
        self.cancel_editing_button.clicked.connect(self.cancel_editing)
        self.layout.addWidget(self.cancel_editing_button)
        self.tabcontainer = QTabWidget(self)
        self.layout.addWidget(self.tabcontainer)

        # Buttons
        self.buttonbox = QDialogButtonBox(self)
        self.apply_button = QPushButton('Apply', self)
        self.apply_button.clicked.connect(self.apply)
        self.apply_button.setDefault(True)
        self.buttonbox.addButton(self.apply_button, QDialogButtonBox.ActionRole)
        self.close_button = QPushButton('Close', self)
        self.close_button.clicked.connect(self.close)
        self.buttonbox.addButton(self.close_button, QDialogButtonBox.RejectRole)
        self.layout.addWidget(self.buttonbox)

        # Tabs, one for each device type
        for devicetype_name, devicetype in DeviceTypeRegistry.items():
            device = devicetype(self)
            self._edit_tab_widgets(device)

    def _edit_tab_widgets(self, device: Device):
        form = QFormLayout()
        container = QWidget(self)
        container.setLayout(form)
        self.field_widgets[device.type] = device.config_widgets()
        for label, widget in self.field_widgets[device.type].items():
            form.addRow(label, widget)
        if device.type in self.tabs:
            # Replace the old tab with the new
            tab_index = self.tabcontainer.indexOf(self.tabs[device.type])
            self.tabcontainer.removeTab(tab_index)
            self.tabs[device.type].deleteLater()
            del self.tabs[device.type]
            self.tabcontainer.insertTab(tab_index, container, device.type)
            self.tabs[device.type] = container
        else:
            # New tab
            self.tabs[device.type] = container
            self.tabcontainer.addTab(container, device.type)

    def cancel_editing(self):
        self._editing_device = None
        self.cancel_editing_button.setVisible(False)

    def edit(self, device: Device):
        self._editing_device = device
        self.cancel_editing_button.setVisible(True)
        self._edit_tab_widgets(device)
        self.tabcontainer.setCurrentWidget(self.tabs[device.type])

    def apply(self):
        devicetype_name = self.tabcontainer.tabText(self.tabcontainer.currentIndex())
        if self._editing_device is None:
            # New device
            device = DeviceTypeRegistry.get(devicetype_name, self.parent())
            device.bad_config.connect(self._bad_config)
            device.configuring.connect(self._device_configuring)
            if not device.set_from_widgets(self.field_widgets[devicetype_name]):
                device.deleteLater()
                return
            self.device_added.emit(device)
        else:
            # Editing device
            self._editing_device.bad_config.connect(self._bad_config)
            self._editing_device.configuring.connect(self._device_configuring)
            if not self._editing_device.set_from_widgets(self.field_widgets[devicetype_name]):
                return
            self.device_edited.emit(self._editing_device)
            self.cancel_editing()

    def _device_configuring(self, message: str):
        if self._progress is None:
            self._progress = QProgressDialog(self)
        if message is not None and len(message) > 0:
            self._progress.setLabelText(message)
            self._progress.setRange(0, 0)
            self._progress.setValue(0)
            self._progress.show()
        else:
            self._progress.close()

    def _bad_config(self, field: str, message: str):
        QMessageBox.warning(self, 'Error in "{field}"'.format(field=field), message)

    def closeEvent(self, event: QCloseEvent):
        self.cancel_editing()
        super().closeEvent(event)
