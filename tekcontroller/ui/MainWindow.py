import pickle
import webbrowser
from pathlib import Path
from typing import Dict, FrozenSet, List

import netifaces
import PySide2
from PySide2.QtCore import QItemSelection, QModelIndex, QPoint, QTimer
from PySide2.QtGui import QKeySequence
from PySide2.QtWidgets import QAction, QActionGroup, QFileDialog, QInputDialog, QMainWindow, QMenu, QMessageBox, QWidget

from devices.device import Device, DeviceAction, DeviceStatus, DeviceTypeRegistry, MessageSeverity
from model.DeviceModel import DeviceModel
from model.LogModel import LogModel
from settings import Settings
from ui.EditDeviceDialog import EditDeviceDialog
from ui.uic.ui_mainwindow import Ui_MainWindow


class MainWindow(QMainWindow):
    """
    Main application window
    """
    app_name = 'Teknopolis Controller'
    _settings = Settings()
    _context_menu: QMenu
    _interface_selectors: Dict[str, QAction] = {}
    _refresh_timer: QTimer = None
    selected_devices: FrozenSet[Device] = []

    def __init__(self, parent: QWidget = None):
        QMainWindow.__init__(self, parent)
        self._init_ui()
        self._init_autorefresh()

    def _init_ui(self):
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle(self.app_name)
        self.setMinimumSize(800, 600)

        # Restore window state
        if self._settings.mainwindow_size is not None:
            self.resize(self._settings.mainwindow_size)
        if self._settings.mainwindow_pos is not None:
            self.move(self._settings.mainwindow_pos)

        self._init_menu()
        self._init_context_menu()
        self._init_table()
        self._init_log()
        self.edit_device_dialog = EditDeviceDialog(self)
        self.edit_device_dialog.device_added.connect(self._device_added)
        self.edit_device_dialog.device_edited.connect(self._device_edited)

        self.ui.startall_button.clicked.connect(self.act_device_start_all)
        self.ui.stopall_button.clicked.connect(self.act_device_stop_all)
        self.ui.refreshall_button.clicked.connect(self.act_device_refresh_all)

        self.act_device_refresh_all()

    def _init_menu(self):
        # File menu
        self.ui.act_import.triggered.connect(self.act_import)
        self.ui.act_export.triggered.connect(self.act_export)
        self.ui.act_exit.setShortcut(QKeySequence.Quit)
        self.ui.act_exit.triggered.connect(self.close)

        # Edit menu
        self.ui.act_start.triggered.connect(self.act_device_start)
        self.ui.act_pause.triggered.connect(self.act_device_pause)
        self.ui.act_stop.triggered.connect(self.act_device_stop)
        self.ui.act_refresh.triggered.connect(self.act_device_refresh)
        self.ui.act_connect.triggered.connect(self.act_device_connect)
        self.ui.act_add.triggered.connect(self.act_add_device)
        self.ui.act_edit.triggered.connect(self.act_edit_device)
        self.ui.act_remove.triggered.connect(self.act_remove_device)

        # Settings menu
        self._init_network_interfaces()
        self.ui.act_refresh_rate.triggered.connect(self.act_refresh_rate)

        # Help menu
        self.ui.act_manual.setShortcut(QKeySequence.HelpContents)
        self.ui.act_manual.triggered.connect(self.act_manual)

        self.ui.insert_button.clicked.connect(self.act_add_device)

    def _init_context_menu(self):
        self._context_menu = self.ui.menu_edit

    def _init_table(self):
        self.model = DeviceModel(self)
        self.ui.table.setModel(self.model)
        self.selection = self.ui.table.selectionModel()
        self.selection.selectionChanged.connect(self._selection_changed)
        self.ui.table.customContextMenuRequested.connect(self.table_context_menu)

        # Load devices from settings
        for device_id, device_settings in self._settings.devices.items():
            device = DeviceTypeRegistry.get(device_settings['type'], self)
            device.settings_load(device_settings)
            device.id = device_id
            self._device_added(device)

    def _init_log(self):
        self.log = LogModel(self)
        self.ui.log.setModel(self.log)

    def _init_network_interfaces(self):
        # Get a dict of interfaces and their IP Addresses
        interfaces: Dict[str, List[str]] = {}
        default = None
        for interface in netifaces.interfaces():
            addresses = []
            interface_addresses = netifaces.ifaddresses(interface)
            if netifaces.AF_INET not in interface_addresses:
                continue
            for info in interface_addresses[netifaces.AF_INET]:
                # Set the default as the first non-loopback interface
                if default is None and info['addr'] != '127.0.0.1':
                    default = interface
                addresses.append(info['addr'])
            interfaces[interface] = addresses

        if self._settings.interface is None:
            self._settings.interface = default

        # Add the menu options
        self.interface_act_group = QActionGroup(self)
        self.interface_act_group.triggered.connect(self.act_interface_changed)
        self.interface_act_group.setExclusive(True)
        for interface, addresses in interfaces.items():
            self._interface_selectors[interface] = QAction('{interface} ({addresses})'.format(
                interface=interface, addresses=', '.join(addresses)), self)
            self._interface_selectors[interface].setCheckable(True)
            self._interface_selectors[interface].setData(interface)
            if interface == self._settings.interface:
                self._interface_selectors[interface].setChecked(True)
            else:
                self._interface_selectors[interface].setChecked(False)
            self.interface_act_group.addAction(self._interface_selectors[interface])
        self.ui.menu_network.addActions(self.interface_act_group.actions())

    def _init_autorefresh(self):
        if self._refresh_timer is None:
            self._refresh_timer = QTimer(self)
            self._refresh_timer.timeout.connect(self.act_device_refresh_all)
        if self._settings.refresh_rate == 0:
            if self._refresh_timer.isActive():
                self._refresh_timer.stop()
        else:
            self._refresh_timer.setInterval(self._settings.refresh_rate * 60 * 1000)
            self._refresh_timer.start()

    def act_import(self):
        filename = QFileDialog.getOpenFileName(self, 'Import devices', str(Path.home()), '*.tek')[0]

        if len(filename) == 0:
            return

        with open(filename, 'rb') as f:
            if not f.readable():
                QMessageBox.critical(self, 'Cannot read file', 'The file cannot be read.')
                return
            self._settings.devices = pickle.load(f)

        self._init_table()

    def act_export(self):
        filename = QFileDialog.getSaveFileName(self, 'Export devices', str(Path.home()), '*.tek')[0]

        if len(filename) == 0:
            return

        with open(filename, 'wb') as f:
            if not f.writable():
                QMessageBox.critical(self, 'Cannot write file', 'The file cannot be written.')
                return
            pickle.dump(self._settings.devices, f)

    def act_refresh_rate(self):
        rate, ok = QInputDialog.getInt(self, 'Auto-Refresh Rate',
                                       'Refresh device status every <value> minutes (0 disables this feature)',
                                       value=self._settings.refresh_rate, minValue=0)
        if ok:
            # Avoid restarting the timer unnecessarily
            old_rate = self._settings.refresh_rate
            if old_rate != rate:
                self._settings.refresh_rate = rate
                self._init_autorefresh()

    def act_manual(self):
        webbrowser.open('https://teknopolis.gitlab.io/controller')

    def act_add_device(self):
        self.edit_device_dialog.show()

    def act_edit_device(self):
        device = self.ui.table.selectedIndexes()[0].data(DeviceModel.DeviceRole)
        self._do_edit(device)

    def act_remove_device(self):
        indexes = self.ui.table.selectedIndexes()
        if len(indexes) == 0:
            return

        msg_box = QMessageBox(self)
        msg_box.setIcon(QMessageBox.Question)
        msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        devices: Dict[int, Device] = {}
        for index in indexes:
            device = index.data(DeviceModel.DeviceRole)
            devices[device.id] = device
        if len(devices) == 1:
            msg_box.setWindowTitle('Remove device?')
            msg_box.setText('Are you sure you want to remove this device?')
        else:
            device_names = []
            for device in devices.values():
                device_names.append(device.name)
            msg_box.setWindowTitle('Remove devices?')
            msg_box.setText('Are you sure you want to remove these devices?')
            msg_box.setInformativeText('<ul><li>' + '</li><li>'.join(device_names) + '</li></ul>')

        if msg_box.exec_() == QMessageBox.Yes:
            for device in devices.values():
                self._do_remove(device)

    def act_device_start(self):
        for device in self.selected_devices:
            if DeviceAction.Start in device.supports():
                device.start()

    def act_device_start_all(self):
        for device in self.model.all_devices():
            if DeviceAction.Start in device.supports():
                device.start()

    def act_device_pause(self):
        for device in self.selected_devices:
            if DeviceAction.Pause in device.supports():
                device.pause()

    def act_device_stop(self):
        # Find all devices that are stoppable
        stoppable_devices = []
        for device in self.selected_devices:
            if DeviceAction.Stop in device.supports():
                stoppable_devices.append(device)
        if len(stoppable_devices) == 0:
            return

        if len(stoppable_devices) == 1:
            title = "Stop device?"
            message = "Are you sure you want to stop this device?"
        else:
            title = "Stop devices?"
            message = "Are you sure you want to stop these devices?"
        if QMessageBox.question(self, title, message) == QMessageBox.Yes:
            for device in stoppable_devices:
                device.stop()

    def act_device_stop_all(self):
        if QMessageBox.question(self, 'Stop all devices?',
                                'Are you sure you want to stop all devices?') == QMessageBox.Yes:
            for device in self.model.all_devices():
                if DeviceAction.Stop in device.supports():
                    device.stop()

    def act_device_refresh(self):
        self._refresh_timer.stop()
        self._init_autorefresh()
        for device in self.selected_devices:
            if DeviceAction.Status in device.supports():
                device.refresh_status()

    def act_device_refresh_all(self):
        if self._refresh_timer is not None:
            self._refresh_timer.stop()
        self._init_autorefresh()
        for device in self.model.all_devices():
            if DeviceAction.Status in device.supports():
                device.refresh_status()

    def act_device_connect(self):
        for device in self.selected_devices:
            if DeviceAction.Remote in device.supports():
                device.remote()
                # Only connect to one device at a time.
                return

    def act_interface_changed(self, action: QAction):
        interface = action.data()
        self._settings.interface = interface

    def table_context_menu(self, click_pos: QPoint):
        self._context_menu.popup(self.ui.table.viewport().mapToGlobal(click_pos))

    def _do_edit(self, device: Device):
        self.edit_device_dialog.edit(device)
        self.edit_device_dialog.show()

    def closeEvent(self, event: PySide2.QtGui.QCloseEvent):
        self._settings.mainwindow_pos = self.pos()
        self._settings.mainwindow_size = self.size()
        super().closeEvent(event)

    def _device_remove(self, device: Device):
        confirmed = QMessageBox.question(self, 'Delete device?',
                                         'Are you sure you want to delete the device "{name}"?'.format(
                                             name=device.name))
        if confirmed == QMessageBox.StandardButton.Yes:
            self._do_remove(device)

    def _do_remove(self, device):
        self.model.remove_device(device)
        device_list = self._settings.devices
        del device_list[device.id]
        self._settings.devices = device_list
        device.deleteLater()

    def _device_added(self, device: Device):
        """
        Add a device to the model and save it to settings
        :param device:
        :return:
        """
        self._update_device(device)
        self.model.add_device(device)
        self._setup_device_actions(device)

    def _device_edited(self, device: Device):
        """
        Tell the model the device has changed and save it to settings
        :param device:
        :return:
        """
        self._update_device(device)
        self.model.device_state_changed(device)

    def _update_device(self, device: Device):
        """
        Save the device to settings, setting the id if needed
        :param device:
        """
        device_list = self._settings.devices
        if device.id is None:
            # Brand new device
            device.id = max(device_list.keys(), default=0) + 1
        device_id = device.id
        device_list[device_id] = device.settings_save()
        self._settings.devices = device_list

    def _setup_device_actions(self, device: Device):
        """
        Connect the appropriate signals for a new device
        :param device:
        :return:
        """
        device.status_changed.connect(self._device_status_changed)
        device.message.connect(self._device_message)

    def _device_status_changed(self, device_id: int, status: int, is_state_change: bool):
        device = self.model.get_device(device_id)
        self.model.device_state_changed(device)
        status = DeviceStatus(status)
        if status != DeviceStatus.Working and is_state_change:
            self.log.add_entry(device, 'Device is now {status}.'.format(status=status.name.lower()),
                               icon=DeviceModel.status_icon_map.get(status))
            self.ui.log.resizeRowToContents(0)

    def _device_message(self, device_id, severity: MessageSeverity, message: str):
        device = self.model.get_device(device_id)
        self.log.add_entry(device, message, severity)
        self.ui.log.resizeRowToContents(0)

    def _selection_changed(self, selected: QItemSelection, deselected: QItemSelection):
        selected = self.selection.selection()
        self.ui.act_start.setEnabled(False)
        self.ui.act_pause.setEnabled(False)
        self.ui.act_stop.setEnabled(False)
        self.ui.act_refresh.setEnabled(False)
        self.ui.act_connect.setEnabled(False)
        self.ui.act_edit.setEnabled(False)
        self.ui.act_remove.setEnabled(False)
        if selected.isEmpty():
            return

        indexes: List[QModelIndex] = selected.indexes()

        # Find actions common to all selected devices
        actions = set(DeviceAction.__members__.values())
        devices = {}
        for index in indexes:
            device: Device = index.data(DeviceModel.DeviceRole)
            devices[device.id] = device
            actions.intersection_update(device.supports())
        self.selected_devices = frozenset(devices.values())

        # Enable proper items
        if DeviceAction.Start in actions:
            self.ui.act_start.setEnabled(True)
        if DeviceAction.Pause in actions:
            self.ui.act_pause.setEnabled(True)
        if DeviceAction.Stop in actions:
            self.ui.act_stop.setEnabled(True)
        if DeviceAction.Status in actions:
            self.ui.act_refresh.setEnabled(True)
        if DeviceAction.Remote in actions and len(devices) == 1:
            self.ui.act_connect.setEnabled(True)

        # Only allow editing/connecting to one device at a time
        if len(devices) == 1:
            self.ui.act_edit.setEnabled(True)

        # Always allow removing when there's a selection
        self.ui.act_remove.setEnabled(True)
