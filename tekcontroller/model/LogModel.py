from dataclasses import dataclass
from datetime import datetime, timezone
from typing import Callable, Dict, List

import dateutil.tz
from PySide2.QtCore import QAbstractTableModel, QModelIndex, QObject, Qt
from PySide2.QtGui import QIcon

from devices.device import Device, MessageSeverity


@dataclass
class LogItem:
    device: Device
    severity: MessageSeverity
    message: str
    icon: QIcon = None
    timestamp: datetime = datetime.now(timezone.utc)


class LogModel(QAbstractTableModel):
    """
    Model class for the log table
    """
    _log: List[LogItem] = []
    _columns: Dict[str, Callable] = {
        'Time': lambda item: item.timestamp.astimezone(dateutil.tz.tzlocal()).strftime('%c'),
        'Severity': lambda item: item.severity,
        'Device': lambda item: item.device.name,
        'Message': lambda item: item.message,
    }

    def __init__(self, parent: QObject = None):
        QAbstractTableModel.__init__(self, parent)

    def add_entry(self, device: Device, message: str, severity: MessageSeverity = MessageSeverity.Info,
                  icon: QIcon = None):
        self.beginInsertRows(QModelIndex(), 0, 0)
        item = LogItem(device=device, severity=MessageSeverity(severity), message=message, icon=icon,
                       timestamp=datetime.now(timezone.utc))
        self._log.insert(0, item)
        self.endInsertRows()

    def rowCount(self, parent: QModelIndex = QModelIndex()) -> int:
        return len(self._log)

    def columnCount(self, parent: QModelIndex = QModelIndex()) -> int:
        return len(self._columns)

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = -1):
        if orientation == Qt.Orientation.Horizontal:
            if role == Qt.DisplayRole:
                return self.col_name(section)

        return None

    @staticmethod
    def col_name(column_index):
        return list(LogModel._columns.keys())[column_index]

    def data(self, index: QModelIndex, role: int = -1):
        if role == Qt.DisplayRole:
            item = self._log[index.row()]
            column_name = self.col_name(index.column())
            value = self._columns[column_name](item)
            if column_name == 'Severity':
                return value.name
            else:
                return value

        if role == Qt.DecorationRole:
            item = self._log[index.row()]
            column_name = self.col_name(index.column())
            value = self._columns[column_name](item)
            if column_name == 'Severity':
                if value.name == 'Info':
                    return QIcon(':/icons/status/message-info')
                elif value.name == 'Warning':
                    return QIcon(':/icons/status/message-warning')
                elif value.name == 'Error':
                    return QIcon(':/icons/status/message-error')
            if column_name == 'Message':
                return item.icon

        return None
