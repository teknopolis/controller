from operator import attrgetter
from typing import Callable, Dict, List

from PySide2.QtCore import QAbstractTableModel, QModelIndex, QObject, Qt
from PySide2.QtGui import QIcon

from devices.device import Device, DeviceStatus


class DeviceModel(QAbstractTableModel):
    """
    Model for displaying devices
    """
    _devices: List[Device] = []
    _columns: Dict[str, Callable] = {
        'Status': lambda device: device.status,
        'Type': lambda device: device.type,
        'Name': lambda device: device.name,
    }
    status_icon_map = {
        DeviceStatus.Unknown: QIcon(':/icons/status/device-unknown'),
        DeviceStatus.Started: QIcon(':/icons/status/device-started'),
        DeviceStatus.Paused: QIcon(':/icons/status/device-paused'),
        DeviceStatus.Stopped: QIcon(':/icons/status/device-stopped'),
        DeviceStatus.Working: QIcon(':/icons/status/device-working'),
        DeviceStatus.NotReady: QIcon(':/icons/status/device-notready'),
    }

    DeviceRole = 10001

    def __init__(self, parent: QObject = None):
        QAbstractTableModel.__init__(self, parent)

    def add_device(self, device: Device):
        """
        Add a device to the model
        :param device:
        """
        self.beginResetModel()
        self._devices.append(device)
        self._devices.sort(key=attrgetter('name'))
        self.endResetModel()

    def remove_device(self, device: Device):
        """
        Remove a device from the model
        :param device:
        """
        row = self._devices.index(device)
        self.beginResetModel()
        del self._devices[row]
        self._devices.sort(key=attrgetter('name'))
        self.endResetModel()

    def device_state_changed(self, device: Device):
        """
        Handle refreshing a device's display by id
        :param device:
        """
        row = self._devices.index(device)
        self.dataChanged.emit(self.createIndex(row, 0), self.createIndex(row, self.columnCount() - 1))

    def get_device(self, device_id: int) -> Device:
        """
        Get a device from its id
        :param device_id:
        :return:
        :raises ValueError: when no device exists with the given id.
        """
        for device in self._devices:
            if device.id == device_id:
                return device

        raise ValueError('No device exists with id "%s".' % device_id)

    def all_devices(self) -> List[Device]:
        return self._devices

    def rowCount(self, parent: QModelIndex = QModelIndex()) -> int:
        return len(self._devices)

    def columnCount(self, parent: QModelIndex = QModelIndex()) -> int:
        return len(self._columns)

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = -1):
        if orientation == Qt.Horizontal:
            if role == Qt.DisplayRole:
                return self.col_name(section)

        return None

    @staticmethod
    def col_name(column_index):
        return list(DeviceModel._columns.keys())[column_index]

    def data(self, index: QModelIndex, role: int = -1):
        if not index.isValid():
            return None

        device = self._devices[index.row()]
        if role == Qt.DisplayRole:
            col_name = self.col_name(index.column())
            value = self._columns[col_name](device)
            if col_name == 'Status':
                value = DeviceStatus(value)
                return value.name
            else:
                return value
        if role == Qt.DecorationRole:
            col_name = self.col_name(index.column())
            value = self._columns[col_name](device)
            if col_name == 'Status':
                return self.status_icon_map.get(value, None)
        if role == DeviceModel.DeviceRole:
            return device

        return None
