import sys

from PySide2.QtCore import QTimer
from PySide2.QtWidgets import QApplication

from resources.qt import icons_rcc

from ui.MainWindow import MainWindow

app = QApplication(sys.argv)

mainwindow = MainWindow()
mainwindow.show()

# Keep Python in Qt's event loop
timer = QTimer()
timer.timeout.connect(lambda: None)
timer.start(100)

sys.exit(app.exec_())
