import enum
import ipaddress
import platform
import socket
import subprocess
import time
from datetime import datetime, timedelta, timezone
from ipaddress import IPv4Address, IPv6Address
from time import sleep
from typing import Dict, List, Union, Optional, Any

import paramiko
import pypsexec.client
import wakeonlan
from class_registry import ClassRegistry
from getmac import get_mac_address
from pjlink import Projector, projector
from pjlink.projector import ProjectorError
from PySide2.QtCore import QObject, QRegExp, QRunnable, QThreadPool, QUrl, Signal, Slot
from PySide2.QtGui import QDesktopServices, QIntValidator, QRegExpValidator
from PySide2.QtWidgets import QLineEdit, QSpinBox, QWidget


@enum.unique
class DeviceAction(enum.IntEnum):
    """
    Device actions
    """
    Start = enum.auto()
    Pause = enum.auto()
    Stop = enum.auto()
    Status = enum.auto()
    Remote = enum.auto()


@enum.unique
class MessageSeverity(enum.IntEnum):
    """
    Message severity classes
    """
    Info = enum.auto()
    Warning = enum.auto()
    Error = enum.auto()


@enum.unique
class DeviceStatus(enum.IntEnum):
    Unknown = enum.auto()
    NotReady = enum.auto()
    Working = enum.auto()
    Started = enum.auto()
    Paused = enum.auto()
    Stopped = enum.auto()


def _ping(host: str, timeout=3) -> bool:
    """
    Ping the given host
    :param host:
    :return: True if the host responds, false if not.
    """
    # Option for the number of packets as a function of
    param = '-n' if platform.system().lower() == 'windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    timeout_time = datetime.now(timezone.utc) + timedelta(seconds=timeout)
    rc = -1
    while rc != 0 and datetime.now(timezone.utc) < timeout_time:
        try:
            rc = subprocess.run(command, capture_output=True, timeout=timeout).returncode
        except subprocess.TimeoutExpired as e:
            rc = -1

    return rc == 0


class DeviceWorkerSignals(QObject):
    status_changed = Signal(int)
    message = Signal(int, str)


class DeviceWorker(QRunnable):
    def __init__(self, action: DeviceAction, settings: Dict[str, Any]):
        QRunnable.__init__(self)
        self.action = action
        self.delay = settings['delay']
        self.timeout = settings['timeout']
        self.signals = DeviceWorkerSignals()

    @Slot()
    def run(self):
        if self.action == DeviceAction.Start:
            self._do_start()
        elif self.action == DeviceAction.Pause:
            self._do_pause()
        elif self.action == DeviceAction.Stop:
            self._do_stop()
        elif self.action == DeviceAction.Status:
            self._do_status()
        elif self.action == DeviceAction.Remote:
            self._do_remote()

    def _do_start(self):
        """
        Perform start functions

        This will be executed from a separate thread.
        """
        self.signals.message.emit(MessageSeverity.Error, 'Not implemented')

    def _do_pause(self):
        """
         Perform pause functions

         This will be executed from a separate thread.
         """
        self.signals.message.emit(MessageSeverity.Error, 'Not implemented')

    def _do_stop(self):
        """
        Perform stop functions

        This will be executed from a separate thread.
        """
        self.signals.message.emit(MessageSeverity.Error, 'Not implemented')

    def _do_status(self):
        """
        Query the device for its status

        This will be executed from a separate thread.
        """
        self.signals.message.emit(MessageSeverity.Error, 'Not implemented')

    def _do_remote(self):
        """
        Remotely connect to the device.

        This will be executed from a separate thread.
        """
        self.signals.message.emit(MessageSeverity.Error, 'Not implemented')


class Device(QObject):
    """
    Device object

    Devices handle the starting and stopping of themselves.
    """
    id: int = None
    name: str = None
    delay: int = 0
    timeout: int = 10
    _old_status: DeviceStatus = DeviceStatus.NotReady
    status: DeviceStatus = DeviceStatus.NotReady
    current_message: str = ''
    type: str = 'Not set'

    # Helpful validator for edit widgets
    not_empty_validator = QRegExpValidator(QRegExp('^.+$'))

    # Signals
    # Explains the configuration step in progress
    configuring = Signal(str)
    # The field and error message when the configuration is bad
    bad_config = Signal(str, str)
    # Device id, DeviceStatus, is this a state change (e.g. started to stopped)
    status_changed = Signal(int, int, bool)
    # Device id, MessageSeverity, and message
    message = Signal(int, int, str)

    def __init__(self, parent: QObject = None):
        QObject.__init__(self, parent)

    @staticmethod
    def supports() -> List[DeviceAction]:
        """
        Get a list of actions this device supports
        """
        return [
            DeviceAction.Start,
            DeviceAction.Stop,
            DeviceAction.Status
        ]

    def worker(self, action) -> DeviceWorker:
        """
        Get the worker class for this device
        :param action:
        """
        return DeviceWorker(action, self.timeout)

    def config_widgets(self) -> Dict[str, QWidget]:
        """
        Create a dict of widgets used to configure exhibits

        :return: A dict with the field name as the key and the widget as the value.
        """
        widgets = {}

        widgets['Name'] = QLineEdit(self.name)
        widgets['Name'].setValidator(self.not_empty_validator)
        widgets['Delay'] = QSpinBox()
        widgets['Delay'].setValue(self.delay)
        widgets['Delay'].setMinimum(0)
        widgets['Timeout'] = QSpinBox()
        widgets['Timeout'].setValue(self.timeout)
        widgets['Timeout'].setMinimum(0)
        widgets['Timeout'].setMaximum(600)

        return widgets

    def set_from_widgets(self, widgets: Dict[str, QWidget]) -> bool:
        """
        Set config from the list of widgets.

        The dict keys are the same as returned from config_widgets()

        :param widgets:
        """
        if not widgets['Name'].hasAcceptableInput():
            self.bad_config.emit('Name', 'Name cannot be blank.')
            return False
        self.name = widgets['Name'].text()
        if not widgets['Timeout'].hasAcceptableInput():
            self.bad_config.emit('Timeout', 'Timeout is out of bounds (0-600)')
            return False
        self.timeout = widgets['Timeout'].value()
        if not widgets['Delay'].hasAcceptableInput():
            self.bad_config.emit('Delay', 'Delay is out of bounds (minimum 0)')
            return False
        self.delay = widgets['Delay'].value()

        return True

    def _do_action(self, action: DeviceAction):
        """
        Perform the action in a separate thread
        """
        if action != DeviceAction.Remote:
            self._old_status = self.status
            self._set_status(DeviceStatus.Working)
        worker = self.worker(action)
        worker.signals.message.connect(self._message_changed)
        worker.signals.status_changed.connect(self._status_changed)
        QThreadPool.globalInstance().start(worker)

    def _status_changed(self, status: DeviceStatus):
        is_state_change = status != self._old_status
        self.status = status
        self.current_message = ''
        self.status_changed.emit(self.id, status, is_state_change)

    def _message_changed(self, severity: MessageSeverity, message: str):
        if message != self.current_message:
            self.current_message = message
            self.message.emit(self.id, severity, message)

    @Slot()
    def start(self):
        """
        Perform all actions needed to start the device
        """
        if self.status != DeviceStatus.Started:
            self._do_action(DeviceAction.Start)

    @Slot()
    def pause(self):
        """
        Perform all actions needed to "pause" the device.

        The meaning may vary from device to device.  For example, a projector
        may close its shutter but not turn the lamp off.
        """
        if self.status != DeviceStatus.Paused and self.status != DeviceStatus.Stopped:
            self._do_action(DeviceAction.Pause)

    @Slot()
    def stop(self):
        """
        Perform all actions needed to shutdown the device.
        """
        if self.status != DeviceStatus.Stopped:
            self._do_action(DeviceAction.Stop)

    @Slot()
    def remote(self):
        """
        Remotely connect to the device.
        """
        self._do_action(DeviceAction.Remote)

    @Slot()
    def refresh_status(self):
        """
        Refresh the device's status
        """
        self._do_action(DeviceAction.Status)

    def settings_save(self) -> Dict:
        """
        Export settings needed to initialize the device configuration
        :return:
        """
        settings = {
            'type': self.type,
            'name': self.name,
            'delay': self.delay,
            'timeout': self.timeout
        }

        return settings

    def settings_load(self, settings: Dict):
        """
        Set device configuration from the given dict of settings.

        This is the same format as returned from settings_save()
        :param settings:
        """
        self.name = settings['name']
        self.delay = settings.get('delay', self.delay)
        self.timeout = settings.get('timeout', self.timeout)

    def _get_mac_address(self, ip_address: Union[IPv4Address, IPv6Address]) -> Optional[str]:
        """
        Get a MAC Address from an IP Address
        :param ip_address:
        :return:
        """
        if _ping(str(ip_address)) == False:
            # Machine is not available, return  nothing.
            return ''
        self.configuring.emit('Configuring MAC address')
        if isinstance(ip_address, IPv4Address):
            mac_address = get_mac_address(ip=str(ip_address), network_request=True)
        else:
            mac_address = get_mac_address(ip6=str(ip_address), network_request=True)
        self.configuring.emit('')
        return mac_address

    def _set_status(self, status: DeviceStatus):
        """
        Change the device's status and tell everyone about it.
        :param status:
        """
        is_state_change = status != self._old_status
        self.status = status
        self.status_changed.emit(self.id, self.status, is_state_change)


DeviceTypeRegistry: Union[ClassRegistry, Dict[str, Device]] = ClassRegistry()


class WindowsWorker(DeviceWorker):
    def __init__(self, action: DeviceAction, settings: Dict[str, Any]):
        DeviceWorker.__init__(self, action, settings)
        self.ip_address = settings['ip_address']
        self.mac_address = settings['mac_address']
        self.username = settings['username']
        self.password = settings['password']

    def _do_start(self):
        if self.mac_address is None or len(self.mac_address) == 0 or self.mac_address == '00:00:00:00:00:00':
            self.signals.message.emit(MessageSeverity.Error, 'No MAC Address is set!')
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return
        time.sleep(self.delay)

        # Wake on lan, then wait a reasonable amount of time
        wakeonlan.send_magic_packet(self.mac_address, ip_address=str(self.ip_address))
        if not _ping(str(self.ip_address), self.timeout):
            self.signals.message.emit(MessageSeverity.Error, 'Host is not reachable.')
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return
        if not self._connectable():
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return
        self.signals.status_changed.emit(DeviceStatus.Started)

    def _connectable(self) -> bool:
        """
        Try to establish a connection
        """
        psclient = pypsexec.client.Client(str(self.ip_address), username=self.username, password=self.password)
        try:
            psclient.connect()
        except Exception as e:
            self.signals.message.emit(MessageSeverity.Error, str(e))
            return False
        try:
            psclient.create_service()
            stdout, stderr, rc = psclient.run_executable("whoami.exe")
            if rc != 0:
                self.signals.message.emit(MessageSeverity.Error, 'Sanity check failed: "{error}"'.format(
                    error="\n".join([stdout, stderr])))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return False
        except Exception as e:
            self.signals.message.emit(MessageSeverity.Error, str(e))
            return False
        finally:
            psclient.cleanup()
            psclient.disconnect()

        return True

    def _do_stop(self):
        psclient = pypsexec.client.Client(str(self.ip_address), username=self.username, password=self.password)
        try:
            psclient.connect()
        except Exception as e:
            self.signals.message.emit(MessageSeverity.Error, str(e))
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return
        try:
            psclient.create_service()
            stdout, stderr, rc = psclient.run_executable("shutdown.exe", arguments='/s /t 0', use_system_account=True)
            if rc != 0:
                self.signals.message.emit(MessageSeverity.Error, 'Error shutting down: "{error}"'.format(
                    error="\n".join([stdout, stderr])))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
        except Exception as e:
            self.signals.message.emit(MessageSeverity.Error, str(e))
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return
        finally:
            psclient.cleanup()
            psclient.disconnect()

        # Wait for the device to drop off the network
        while _ping(str(self.ip_address), 3):
            sleep(1)
        self.signals.status_changed.emit(DeviceStatus.Stopped)

    def _do_status(self):
        if not _ping(str(self.ip_address), 3):
            self.signals.status_changed.emit(DeviceStatus.Stopped)
            return

        if not self._connectable():
            self.signals.status_changed.emit(DeviceStatus.NotReady)
            return

        self.signals.status_changed.emit(DeviceStatus.Started)

    def _do_remote(self):
        url = QUrl('vnc://%s' % str(self.ip_address))
        QDesktopServices.openUrl(url)


@DeviceTypeRegistry.register('Windows PC')
class Windows(Device):
    type = 'Windows PC'
    ip_address: Union[IPv4Address, IPv6Address] = ''
    mac_address: str = None
    username: str = None
    password: str = None

    def __init__(self, parent: QObject = None):
        Device.__init__(self, parent)

    @staticmethod
    def supports() -> List[DeviceAction]:
        return [
            DeviceAction.Start,
            DeviceAction.Stop,
            DeviceAction.Status,
            DeviceAction.Remote
        ]

    def worker(self, action) -> DeviceWorker:
        return WindowsWorker(action, self.settings_save())

    def config_widgets(self) -> Dict[str, QWidget]:
        widgets = super().config_widgets()
        widgets['IP Address'] = QLineEdit(str(self.ip_address))
        # widgets['IP Address'].setInputMask('000.000.000.000;_')
        ip_validator = QRegExpValidator(QRegExp('^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'))
        widgets['IP Address'].setValidator(ip_validator)
        widgets['MAC Address'] = QLineEdit(self.mac_address)
        # widgets['MAC Address'].setInputMask('HH:HH:HH:HH:HH:HH;_')
        mac_validator = QRegExpValidator(
            QRegExp('^([\dA-Fa-f]{2}:[\dA-Fa-f]{2}:[\dA-Fa-f]{2}:[\dA-Fa-f]{2}:[\dA-Fa-f]{2}:[\dA-Fa-f]{2})?$'))
        widgets['MAC Address'].setValidator(mac_validator)
        widgets['MAC Address'].setPlaceholderText('Auto (machine must be on)')
        widgets['Username'] = QLineEdit(self.username)
        widgets['Password'] = QLineEdit(self.password)
        widgets['Password'].setEchoMode(QLineEdit.Password)

        return widgets

    def set_from_widgets(self, widgets: Dict[str, QWidget]) -> bool:
        if not super().set_from_widgets(widgets):
            return False
        if not widgets['IP Address'].hasAcceptableInput():
            self.bad_config.emit('IP Address', 'IP Address is not valid')
            return False
        self.ip_address = ipaddress.ip_address(widgets['IP Address'].text())
        if len(widgets['MAC Address'].text()) > 0 and not widgets['MAC Address'].hasAcceptableInput():
            self.bad_config.emit('MAC Address', 'MAC Address is not valid')
            return False
        self.mac_address = widgets['MAC Address'].text()
        self.username = widgets['Username'].text()
        self.password = widgets['Password'].text()

        if len(self.mac_address) == 0:
            self.mac_address = self._get_mac_address(self.ip_address)
            if self.mac_address is None:
                # Couldn't get MAC Address
                self.bad_config.emit('MAC Address', 'Could not automatically discover MAC Address.')
                return False

        return True

    def settings_save(self) -> Dict:
        settings = super().settings_save()
        settings['ip_address'] = self.ip_address
        settings['mac_address'] = self.mac_address
        settings['username'] = self.username
        settings['password'] = self.password

        return settings

    def settings_load(self, settings: Dict):
        super().settings_load(settings)
        self.ip_address = settings['ip_address']
        self.mac_address = settings['mac_address']
        self.username = settings['username']
        self.password = settings['password']


class PosixWorker(DeviceWorker):
    def __init__(self, action: DeviceAction, settings: Dict[str, Any]):
        DeviceWorker.__init__(self, action, settings)
        self.ip_address = settings['ip_address']
        self.mac_address = settings['mac_address']
        self.username = settings['username']
        self.password = settings['password']

    def _do_start(self):
        if self.mac_address is None or len(self.mac_address) == 0 or self.mac_address == '00:00:00:00:00:00':
            self.signals.message.emit(MessageSeverity.Error, 'No MAC Address is set!')
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return
        time.sleep(self.delay)

        # Wake on lan, then wait a reasonable amount of time
        wakeonlan.send_magic_packet(self.mac_address, ip_address=str(self.ip_address))
        if not _ping(str(self.ip_address), timeout=self.timeout):
            self.signals.message.emit(MessageSeverity.Error, 'Host is not reachable.')
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return
        if not self._connectable():
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return
        self._startup()
        self.signals.status_changed.emit(DeviceStatus.Started)

    def _startup(self):
        pass

    def _connectable(self) -> bool:
        """
        Establish a test connection with the device
        """
        output, error = self._ssh_command('id -un')
        if output != self.username:
            self.signals.message.emit(MessageSeverity.Error, 'Bad response from host')
            return False

        return True

    def _ssh_command(self, command: str):
        client = None
        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy)
            client.connect(str(self.ip_address), username=self.username, password=self.password, timeout=self.timeout)
            stdin, stdout, stderr = client.exec_command(command, timeout=self.timeout)
            output = str(stdout.read(), encoding='utf-8').strip()
            error = str(stderr.read(), encoding='utf-8').strip()
        except paramiko.AuthenticationException as e:
            self.signals.message.emit(MessageSeverity.Error, 'Error authenticating with host')
            output = ''
            error = str(e)
        except Exception as e:
            self.signals.message.emit(MessageSeverity.Error, str(e))
            output = ''
            error = str(e)
        finally:
            if client is not None:
                client.close()

        return output, error

    def _do_stop(self):
        self._shutdown()
        output, error = self._ssh_command('sudo shutdown --poweroff now')
        if len(error) > 0:
            self.signals.message.emit(MessageSeverity.Error, error)
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return

        # Wait for the device to drop off the network
        while _ping(str(self.ip_address), 3):
            sleep(1)

        self.signals.status_changed.emit(DeviceStatus.Stopped)

    def _shutdown(self):
        pass

    def _do_status(self):
        if not _ping(str(self.ip_address), 3):
            self.signals.status_changed.emit(DeviceStatus.Stopped)
            return

        if not self._connectable():
            self.signals.status_changed.emit(DeviceStatus.NotReady)
            return

        self.signals.status_changed.emit(DeviceStatus.Started)

    def _do_remote(self):
        url = QUrl('vnc://%s' % str(self.ip_address))
        QDesktopServices.openUrl(url)


class Posix(Device):
    type = 'POSIX PC'
    ip_address: Union[IPv4Address, IPv6Address] = ''
    mac_address: str = None
    username: str = None
    password: str = None

    def __init__(self, parent: QObject = None):
        Device.__init__(self, parent)

    @staticmethod
    def supports() -> List[DeviceAction]:
        return [
            DeviceAction.Start,
            DeviceAction.Stop,
            DeviceAction.Status,
            DeviceAction.Remote
        ]

    def worker(self, action) -> DeviceWorker:
        return PosixWorker(action, self.settings_save())

    def config_widgets(self) -> Dict[str, QWidget]:
        widgets = super().config_widgets()
        widgets['IP Address'] = QLineEdit(str(self.ip_address))
        # widgets['IP Address'].setInputMask('000.000.000.000;_')
        ip_validator = QRegExpValidator(QRegExp('^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'))
        widgets['IP Address'].setValidator(ip_validator)
        widgets['MAC Address'] = QLineEdit(self.mac_address)
        # widgets['MAC Address'].setInputMask('HH:HH:HH:HH:HH:HH;_')
        mac_validator = QRegExpValidator(
            QRegExp('^([\dA-Fa-f]{2}:[\dA-Fa-f]{2}:[\dA-Fa-f]{2}:[\dA-Fa-f]{2}:[\dA-Fa-f]{2}:[\dA-Fa-f]{2})?$'))
        widgets['MAC Address'].setValidator(mac_validator)
        widgets['MAC Address'].setPlaceholderText('Auto (machine must be on)')
        widgets['Username'] = QLineEdit(self.username)
        widgets['Password'] = QLineEdit(self.password)
        widgets['Password'].setEchoMode(QLineEdit.Password)

        return widgets

    def set_from_widgets(self, widgets: Dict[str, QWidget]) -> bool:
        if not super().set_from_widgets(widgets):
            return False
        if not widgets['IP Address'].hasAcceptableInput():
            self.bad_config.emit('IP Address', 'IP Address is not valid')
            return False
        self.ip_address = ipaddress.ip_address(widgets['IP Address'].text())
        if len(widgets['MAC Address'].text()) > 0 and not widgets['MAC Address'].hasAcceptableInput():
            self.bad_config.emit('MAC Address', 'MAC Address is not valid')
            return False
        self.mac_address = widgets['MAC Address'].text()
        self.username = widgets['Username'].text()
        self.password = widgets['Password'].text()

        if len(self.mac_address) == 0:
            self.mac_address = self._get_mac_address(self.ip_address)
            if self.mac_address is None:
                # Couldn't get MAC Address
                self.bad_config.emit('MAC Address', 'Could not automatically discover MAC Address.')
                return False

        return True

    def settings_save(self) -> Dict:
        settings = super().settings_save()
        settings['ip_address'] = self.ip_address
        settings['mac_address'] = self.mac_address
        settings['username'] = self.username
        settings['password'] = self.password

        return settings

    def settings_load(self, settings: Dict):
        super().settings_load(settings)
        self.ip_address = settings['ip_address']
        self.mac_address = settings['mac_address']
        self.username = settings['username']
        self.password = settings['password']


@DeviceTypeRegistry.register('Linux PC')
class Linux(Posix):
    type = 'Linux PC'

    def __init__(self, parent: QObject = None):
        Posix.__init__(self, parent)


class MacWorker(PosixWorker):
    def _startup(self):
        time.sleep(5)
        self._ssh_command('open ~/tek_start.app')

    def _shutdown(self):
        self._ssh_command('open ~/tek_stop.app')
        time.sleep(5)


@DeviceTypeRegistry.register('Mac')
class Mac(Posix):
    type = 'Mac'

    def __init__(self, parent: QObject = None):
        Posix.__init__(self, parent)

    def worker(self, action) -> DeviceWorker:
        return MacWorker(action, self.settings_save())


class PjLinkWorker(DeviceWorker):
    def __init__(self, action: DeviceAction, settings: Dict[str, Any]):
        DeviceWorker.__init__(self, action, settings)
        self.ip_address = settings['ip_address']
        self.port = settings['port']
        self.password = settings['password']

    def _do_start(self):
        time.sleep(self.delay)
        # Projectors can return garbage if queried to quickly.  To work around this
        # race condition, try again if a corrupt message is received.
        succeeded = False
        while not succeeded:
            time.sleep(1)
            try:
                pj = self._get_projector()
                if pj is None:
                    return
                if pj.get_power() != 'on':
                    pj.set_power('on')
                    while pj.get_power() != 'on':
                        time.sleep(5)
                pj.set_mute(projector.MUTE_VIDEO | projector.MUTE_AUDIO, False)
                succeeded = True
            except ValueError:
                continue
            except ProjectorError as e:
                self.signals.message.emit(MessageSeverity.Error, str(e.args[0], encoding='utf-8'))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
            except Exception as e:
                self.signals.message.emit(MessageSeverity.Error, str(e))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
        self.signals.status_changed.emit(DeviceStatus.Started)

    def _get_projector(self):
        try:
            sock = socket.create_connection((str(self.ip_address), self.port), self.timeout)
        except socket.timeout:
            self.signals.message.emit(MessageSeverity.Error, 'Cannot connect to device (timed out)')
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return None
        f = sock.makefile('rwb')
        pj = Projector(f)
        auth = pj.authenticate(lambda: self.password)
        if auth is False:
            self.signals.message.emit(MessageSeverity.Error, 'Authentication error (is password correct?)')
            self.signals.status_changed.emit(DeviceStatus.Unknown)
            return None
        return pj

    def _do_pause(self):
        # Projectors can return garbage if queried to quickly.  To work around this
        # race condition, try again if a corrupt message is received.
        succeeded = False
        while not succeeded:
            time.sleep(1)
            try:
                pj = self._get_projector()
                if pj is None:
                    return
                pj.set_mute(projector.MUTE_VIDEO | projector.MUTE_AUDIO, True)
                succeeded = True
            except ValueError:
                continue
            except ProjectorError as e:
                self.signals.message.emit(MessageSeverity.Error, str(e.args[0], encoding='utf-8'))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
            except Exception as e:
                self.signals.message.emit(MessageSeverity.Error, str(e))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
        self.signals.status_changed.emit(DeviceStatus.Paused)

    def _do_stop(self):
        # Projectors can return garbage if queried to quickly.  To work around this
        # race condition, try again if a corrupt message is received.
        succeeded = False
        while not succeeded:
            time.sleep(1)
            try:
                pj = self._get_projector()
                if pj is None:
                    return
                pj.set_power('off')
                while pj.get_power() != 'off':
                    time.sleep(5)
                succeeded = True
            except ValueError:
                continue
            except ProjectorError as e:
                self.signals.message.emit(MessageSeverity.Error, str(e.args[0], encoding='utf-8'))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
            except Exception as e:
                self.signals.message.emit(MessageSeverity.Error, str(e))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
        self.signals.status_changed.emit(DeviceStatus.Stopped)

    def _do_status(self):
        # Projectors can return garbage if queried to quickly.  To work around this
        # race condition, try again if a corrupt message is received.
        succeeded = False
        while not succeeded:
            time.sleep(1)
            try:
                pj = self._get_projector()
                if pj is None:
                    return
                power = pj.get_power()
                video_mute, audio_mute = pj.get_mute()
                errors = pj.get_errors()
            except ValueError:
                continue
            except ProjectorError as e:
                self.signals.message.emit(MessageSeverity.Error, str(e.args[0], encoding='utf-8'))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
            except Exception as e:
                self.signals.message.emit(MessageSeverity.Error, str(e))
                self.signals.status_changed.emit(DeviceStatus.Unknown)
                return
            succeeded = True

            if power == 'off':
                self.signals.status_changed.emit(DeviceStatus.Stopped)
            elif power == 'on':
                if video_mute or audio_mute:
                    self.signals.status_changed.emit(DeviceStatus.Paused)
                else:
                    self.signals.status_changed.emit(DeviceStatus.Started)
            else:
                self.signals.status_changed.emit(DeviceStatus.NotReady)
                message = 'Projector says Power="{power}", VideoMute="{vmute}", AudioMute="{amute}"'.format(
                    power=power, vmute=video_mute, amute=audio_mute)
                self.signals.message.emit(MessageSeverity.Warning, message)

            # Get and display errors and warnings
            error_messages = []
            warning_count = 0
            error_count = 0
            for category, state in errors.items():
                if state != 'ok':
                    error_messages.append('{cat} {state}'.format(cat=category, state=state))
                    if state == 'error':
                        error_count += 1
                    else:
                        warning_count += 1
            if error_count > 0:
                severity = MessageSeverity.Error
                self.signals.status_changed.emit(DeviceStatus.NotReady)
            else:
                severity = MessageSeverity.Warning
            if warning_count > 0 or error_count > 0:
                self.signals.message.emit(severity, ', '.join(error_messages))


@DeviceTypeRegistry.register('PJLink')
class PjLink(Device):
    type = 'PJLink'
    ip_address: Union[IPv4Address, IPv6Address] = ''
    port = 4352
    password: str = None

    def __init__(self, parent: QObject = None):
        Device.__init__(self, parent)

    @staticmethod
    def supports() -> List[DeviceAction]:
        return [
            DeviceAction.Start,
            DeviceAction.Pause,
            DeviceAction.Stop,
            DeviceAction.Status
        ]

    def worker(self, action) -> DeviceWorker:
        return PjLinkWorker(action, self.settings_save())

    def config_widgets(self) -> Dict[str, QWidget]:
        widgets = super().config_widgets()
        widgets['IP Address'] = QLineEdit(str(self.ip_address))
        # widgets['IP Address'].setInputMask('000.000.000.000;_')
        ip_validator = QRegExpValidator(QRegExp('^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'))
        widgets['IP Address'].setValidator(ip_validator)
        port_validator = QIntValidator(0, 65535)
        widgets['Port'] = QLineEdit(str(self.port))
        widgets['Port'].setValidator(port_validator)
        widgets['Password'] = QLineEdit(self.password)
        widgets['Password'].setEchoMode(QLineEdit.Password)

        return widgets

    def set_from_widgets(self, widgets: Dict[str, QWidget]) -> bool:
        if not super().set_from_widgets(widgets):
            return False
        if not widgets['IP Address'].hasAcceptableInput():
            self.bad_config.emit('IP Address', 'IP Address is not valid')
            return False
        self.ip_address = ipaddress.ip_address(widgets['IP Address'].text())
        if not (widgets['Port'].hasAcceptableInput()):
            self.bad_config.emit('Port', 'Port number is not valid')
            return False
        self.port = int(widgets['Port'].text())
        self.password = widgets['Password'].text()

        return True

    def settings_save(self) -> Dict:
        settings = super().settings_save()
        settings['ip_address'] = self.ip_address
        settings['port'] = self.port
        settings['password'] = self.password

        return settings

    def settings_load(self, settings: Dict):
        super().settings_load(settings)
        self.ip_address = settings['ip_address']
        self.port = settings['port']
        self.password = settings['password']
