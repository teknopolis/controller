import pickle
import packaging.version
from typing import Dict

from PySide2.QtCore import QPoint, QSettings, QSize


class Settings:
    _initialized = False
    _version = '2020.0'
    # Settings keys with default values
    interface: str = None
    refresh_rate: int = 1
    devices: Dict[int, Dict] = {}

    # Application state
    mainwindow_size: QSize = None
    mainwindow_pos: QPoint = None

    def init(self):
        """
        Initialize defaults

        This is called the first time a value is requested or set, as the constructor is loaded too early
        for translation services to be available.
        """
        settings = QSettings('Dan Keenan', 'TeknopolisController')
        # Reset settings if they are an earlier version
        if settings.contains('_version'):
            version = pickle.loads(settings.value('_version'))
            if packaging.version.parse(version) < packaging.version.parse(self._version):
                settings.clear()
        else:
            # Store settings are ancient
            settings.clear()
        settings.setValue('_version', pickle.dumps(self._version))
        object.__setattr__(self, '_initialized', True)

    def __getattribute__(self, name: str):
        if name.startswith('_'):
            return object.__getattribute__(self, name)

        if not object.__getattribute__(self, '_initialized'):
            object.__getattribute__(self, 'init')()
        settings = QSettings('Dan Keenan', 'TeknopolisController')
        if settings.contains(name):
            object.__setattr__(self, name, pickle.loads(settings.value(name)))
        else:
            # Store the default
            settings.setValue(name, pickle.dumps(object.__getattribute__(self, name)))

        return object.__getattribute__(self, name)

    def __setattr__(self, name: str, value):
        if name.startswith('_'):
            return object.__setattr__(self, name, value)

        if not object.__getattribute__(self, '_initialized'):
            object.__getattribute__(self, 'init')()
        settings = QSettings('Dan Keenan', 'TeknopolisController')
        object.__setattr__(self, name, value)
        settings.setValue(name, pickle.dumps(value))
