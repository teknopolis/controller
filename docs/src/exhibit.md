# Exhibit Setup

## Windows
1. Run the installer from [here](files/Teknopolis Exhibit.zip) to setup the
   exhibit machine.
1. Install [TightVNC server](https://www.tightvnc.com/download.php) from their
   website.
1. Setup automatic login:
    1. Press `Windows Key` + `R`.  Type `netplwiz` and click OK.
    1. Uncheck *Users must enter a user name and password to use this computer*
       and click OK.  Now enter the desired username and password and click OK.
       <br>![](img/windows-autologin.png)
1. Enable Wake-on-LAN:
    1. Right-click on the start menu and choose *Device Manager*.
    1. Under *Network Adapters*, find the network card in question.  These names
       vary wildly, and may (or may not) actually have "Ethernet" in the name.
       Most likely, it's the only device that is not a Wi-Fi or Bluetooth adapter.
       Right click on this device and select "Properties".  Depending on the
       network card, these instructions may be slightly different.  However, the
       terminology should be similar.
       <br>![](img/windows-devicemanager.png)
    1. In the "Advanced" tab, scroll in the list to find "Wake on Magic Packet".
       Set this to "Enabled".
       <br>![](img/windows-wol.png)
    1. Reboot the computer and enter the BIOS/UEFI settings.  Methods to do
       this vary, but usually involve repeatedly pressing one of `Delete` or
       `F4`.  Look on the startup screen for a hint to which button to press.
    1. Inside the configuration, hunt for a setting named "Wake on LAN" and/or
       "Wake on Magic Packet".  Turn these on.  There is no standard location
       for these settings; they are often hidden in "Power" or "Hardware"
       settings.
    1. Save changes and exit.
1. Note the following information for configuring the control computer:
    - The exhibit it belongs to.
    - The device IP address ([instructions](https://support.microsoft.com/en-us/help/15291/windows-find-pc-ip-address))
    - The device MAC address (from the "Physical Address" seen above)
       - This can be automatically determined if the exhibit computer is turned on
         and accessible on the network when configuring the control computer.
    - The username and password needed to login to the computer.  This is needed
      even when auto-login is enabled.

## Mac OS
1. Run the installer from [here](files/Teknopolis Exhibit.dmg)
1. Edit the Automator workflows tek_start.app and tek_stop.app in the user's
   home directory to control what happens on startup and shutdown respectively.
1. Make computer wake-up more sane:
    1. Restart the computer and boot into Recovery mode by holding `Cmd`+`R` on boot until
       the Apple logo appears.
    1. Open Terminal from the Utilities menu.
    1. Disable System Integrity Protection by running `csrutil disable`.  Reboot.
    1. After OSX boots up, make sure you're logging in as an administrator and
       open Terminal.
    1. Edit the boot configuration file by running
       `sudo nano /Library/Preferences/SystemConfiguration/com.apple.Boot.plist`.
       Enter your password if prompted
    1. Nano is a simple terminal text editor.  Use the arrow keys to navigate
       the screen.  Change the highlighted section to match.  Don't touch
       anything else!

            :::xml hl_lines="5 6"
            <?xml version="1.0" encoding="UTF-8"?>
            <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
            <plist version="1.0">
            <dict>
                <key>Kernel Flags</key>
                <string>darkwake=0</string>
            </dict>
            </plist>
            
    1. Press `Ctrl`+`O` (for **O**utput) to save and `Ctrl`+`X` to exit.
    1. Reboot into Recovery mode (again) by holding CMD+R on boot time.
    1. Open Terminal from Utilities menu.
    1. Re-enable System Integrity Protection by running `csrutil enable`.
       Reboot again.
    1. After OSX boots up, disable the Password Requirement after sleep, by
       opening *System Preferences* > *Security & Privacy*, selecting the
       *General* tab, and unchecking the *Require password* field.

## Linux
1. Download and extract the archive from [here](files/TeknopolisExhibit.tar.bz2).
1. In a terminal, run `./setup.sh`.
1. In the desktop manager's settings, enable auto login.  See instructions for
   [Ubuntu GNOME](https://help.ubuntu.com/stable/ubuntu-help/user-autologin.html.en)
   and [Kubuntu KDE](https://forum.kde.org/viewtopic.php?f=66&t=156731#p409612).
1. Enable screen sharing:
    - On Kubuntu, install `krfb` and follow [these instructions](https://docs.kde.org/trunk5/en/kdenetwork/krfb/krfb-configuration.html).
      Open `krfb` turn on *Unattended Access*.  Note the password.
    - On Ubuntu, follow [these instructions](https://websiteforstudents.com/access-ubuntu-18-04-lts-beta-desktop-via-vnc-from-windows-machines/).
1. Reboot the computer and enter the BIOS/UEFI settings.  Methods to do
   this vary, but usually involve repeatedly pressing one of `Delete` or
   `F4`.  Look on the startup screen for a hint to which button to press.
1. Inside the configuration, hunt for a setting named "Wake on LAN" and/or
   "Wake on Magic Packet".  Turn these on.  There is no standard location
   for these settings; they are often hidden in "Power" or "Hardware"
   settings.
1. Save changes and exit.
