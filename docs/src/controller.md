# Controller Setup

## Downloads
  - [Windows](files/Teknopolis Controller.zip)
  - [Mac OS](files/Teknopolis Controller.dmg)
  - [Linux](files/TeknopolisController.tar.bz2)

Windows and Linux controllers require a VNC viewer for remote connection
functionality.  Try [TightVNC](https://www.tightvnc.com/download.php) on
Windows and [KRDC](https://kde.org/applications/internet/org.kde.krdc) on Linux.

## Usage
Upon startup the main screen will appear:
<br>![](img/controller-main.png)

The main device table is at the top and lists all of the controlled devices.
Right-click on a device to control or edit it.  The Start/Stop/Refresh all
buttons will apply that action to all devices.

Depending on the controller's operating system, the *Connect* action in the
right-click menu can open the default screen-sharing program.

The log is at the bottom and shows when devices have changed state.  It will also
show if a device has a problem.  It is sorted by time, with the newest entries
at the top.

## Add/Edit devices
Click *Add Device* to open the Add/Edit Device dialog.
<br>![](img/controller-add-device.png)

The various device types are shown as tabs at the top.  Each device type has
slightly different configuration.  Select the device type and enter the data in
the fields.  Click *Apply* to add the device.  The dialog will remain open and
additional devices may be added until you click *Close*.

To edit a device, right click on it and choose *Edit*.  The Add/Edit Device will
open in Edit Mode, notated by a large button at the top of the window.  When in
edit mode, clicking *Apply* will change the last device selected for editing.  The
dialog will then revert back to Add Mode.

To cancel Edit Mode, click the button at the top of the dialog.  This button is
only shown while in Edit Mode.

### Windows/Mac/Linux
These devices use the same parameters:

Name
:   Device name, e.g. the exhibit name.  This should be unique to avoid
    confusion.

Timeout
:   The number of seconds to wait before the controller will give up and 
    assume the device has a serious error.  Defaults to 10.
    
IP Address
:   The device's IP Address.  This must be visible on the [selected network](#network-interface).

MAC Address
:   The device's MAC Address.  If this field is left blank and the device is
    available on the network, it can be determined automatically.
    
Username/Password
:   The login username and password to use.  The user must be the platform's
    version of an administrator for the controller to function.
    
### PJLink
PJLink uses most of the same settings as Windows/Mac/Linux computers above.
It also uses:

Port
:   The port to use when communicating with the projector.  This defaults to
    4352, the port number used in the official specification.
    
Password
:   Some projectors allow a configurable password for network control.  If the
    projector has a password, enter is here.  If authentication is disabled, 
    leave this field blank. 

## Settings
### Network Interface
It is important to choose the correct network interface to use for communication.
If the correct interface is not chosen, some devices will not respond correctly.

The *Settings* > *Network Interface* menu lists all interfaces on the controller.
The names are somewhat arbitrary and depend on the operating system.  To help in
selecting the correct interface, the IP Address is shown as well.

### Auto-Refresh Rate
The controller has an auto-refresh feature that will poll all of the devices for
their status at a configurable rate.  Enter the desired refresh rate (in minutes)
here.  Enter `0` to disable auto-refresh.  The *Refresh All* button will always
work without regard to this setting.
