# Teknopolis Controller
[![pipeline status](https://gitlab.com/teknopolis/controller/badges/master/pipeline.svg)](https://gitlab.com/teknopolis/controller/-/commits/master)

Start and stop Windows PCs, Macs, and PJLink devices.

## Downloads

### Control
  - [Windows](files/Teknopolis Controller.zip)
  - [Mac OS](files/Teknopolis Controller.dmg)
  - [Linux](files/TeknopolisController.tar.bz2)
  
### Exhibit
  - [Windows](files/Teknopolis Exhibit.zip)
    - Also install the [TightVNC Server](https://www.tightvnc.com/download.php)
      to enable Screen Sharing.
  - [Mac OS](files/Teknopolis Exhibit.dmg)
  - [Linux](files/TeknopolisExhibit.tar.bz2)
